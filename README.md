# hsestartpage
A web login and authentication app.  Back-end in Node.js with Express framework.  Front-end in React.js.  Authentication by the Passport.js library. Also uses Bootstrap 4, Google Fonts, and a Fontastic.me generated font.  Property of McMaster - HSE (Health Systems Evidence).

## Steps to Install and Run Locally
Make sure you have [Node.js](http://nodejs.org/) and the [Heroku CLI](https://cli.heroku.com/) installed.

- git clone https://gitlab.com/mcmaster_forum/hsestartpage.git 
- npm install
- npm index.js OR nodemon index.js OR node index.js 
- Runs on port 5000 --> http://localhost:5000/ 
- Your app should now be running on [localhost:5000](http://localhost:5000/). 


## Features
- Node.js
- Bootstrap 4
- Fontastic.me custom generated vector icons
- Google Fonts


## License
This project is licensed under the terms of the **MIT** license.

![HSE Start Page](http://ryanhunter.org/portfolio/hse_homepage.png)


## Deploying to Heroku
Make sure you have [Node.js](http://nodejs.org/) and the [Heroku CLI](https://cli.heroku.com/) installed.

```
$ heroku create
$ git push heroku master
$ heroku open
```
or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)



